import java.io.IOException;
import java.util.Scanner;

public class ArithOpt 
{
public static void main(String args[]) throws IOException
{
	double a,b;
	String ch="";
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the number A:");
	a=sc.nextDouble();
	System.out.println("Enter the number B");
	b=sc.nextDouble();
	System.out.println("Enter the option");
	ch=sc.next();
	switch(ch)
	{
	case "Addition":
	{
		System.out.println("Addition of numbers is: "+(a+b));
		break;
	}
	case "Subtraction":
	{
		System.out.println("Subtraction of number is: "+(a-b));
		break;
	}
	case "Multiplication":
	{
		System.out.println("Multiplication of numbers is: "+(a*b));
		break;
	}
	case "Division":
	{
		System.out.println("Division of numbers is: "+(a/b));
		break;
	}
	default:
	{
		System.out.println("Select the correct option. Thank you");
	}
	}
	
}
}
