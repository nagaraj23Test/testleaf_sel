package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel
{

public static Object[][] readData() throws IOException
{
	//Workbook reading
	XSSFWorkbook wb=new XSSFWorkbook("./Data/readExcel.xlsx");
	
	//sheet reading
	//Using Index
	XSSFSheet sheet = wb.getSheetAt(0);
	//row count
	int rowNum = sheet.getLastRowNum();
	//get Rows
	//get Cell count
	int lastCellNum = sheet.getRow(0).getLastCellNum();
	
	//Assigning Object array
	Object[][] data=new Object[rowNum][lastCellNum];
	for(int j=1;j<=rowNum;j++ )
		{
		XSSFRow rows = sheet.getRow(j);
		
          //get Cells
     
     for(int i=0;i<lastCellNum;i++)
     {
     XSSFCell cells = rows.getCell(i);
     try {
     String value = cells.getStringCellValue();
     //System.out.println(value);
     data[j-1][i]=value;
     }
     catch(NullPointerException e)
     {
    	 System.out.println("");
     }
     }
		}
     //close the workbook
     wb.close();
return data;
}
}
