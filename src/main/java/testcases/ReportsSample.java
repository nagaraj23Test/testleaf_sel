package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportsSample
{
	@Test
	public void reportGenertion() throws IOException
	{
//Create a reference for the ExtentHtmlReporter
	ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
	//To view the history of execution
	html.setAppendExisting(true);
	//create a reference for extentreports 
	ExtentReports extent=new ExtentReports();
	extent.attachReporter(html);
	ExtentTest logger=extent.createTest("TC001_CreateLead","Create a new Lead");
	logger.assignAuthor("Nagarajan");
	logger.assignCategory("Smoke");
	logger.log(Status.PASS, "The Data DemoSalesManager Entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./Snap/img1.png").build());
	extent.flush();
	}
}
