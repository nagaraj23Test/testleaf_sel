package testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.SeMethods;
import wdMethods.TestNGTestcases;

public class CreateLeaf extends TestNGTestcases
{
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName = "CreateLeaf";
		testDesc = "TestLeaf Webpage";
		author = "Nagarajan";
		category = "Practice";
	}
	//@Test(invocationCount=2, timeOut=5000)
	//@Test(invocationCount=2, invocationTimeOut=3000)
	//@Test(groups= {"smoke"})
	@Test(invocationCount=1,dataProvider="qa")
	public void createLead(String cName, String fName, String lName, String postCode) {		
		//login();
		WebElement createLead = locateElement("linkText", "Create Lead");
		click(createLead);
		WebElement companyName = locateElement("id","createLeadForm_companyName");
		type(companyName,cName);
		WebElement firstName = locateElement("id","createLeadForm_firstName");
		type(firstName,fName);
		WebElement lastName = locateElement("id","createLeadForm_lastName");
		type(lastName,lName);
		WebElement calendarSel = locateElement("xpath","//img[@id='createLeadForm_birthDate-button']");
	    click(calendarSel);
	    WebElement dateClick = tableSelection("calendar","12");
        click(dateClick);
        
        WebElement postalCode = locateElement("xpath","//input[@name='generalPostalCode']");
	    type(postalCode, postCode); 
        WebElement clickButton = locateElement("xpath","//input[@class='smallSubmit']");
	   click(clickButton);
	    
	}
	    //Test-case below is for Find Leads
	   
	   @DataProvider(name="qa")
	   public Object[][] fetchData() throws IOException
	   {
		 /*  Object[][] data=new Object[3][4];
		   data[0][0]="hcl";
		   data[0][1]="Kumar";
		   data[0][2]="Rajan";
		   data[0][3]=600109;

		   data[1][0]="CTS";
		   data[1][1]="Gokul";
		   data[1][2]="Adhi";
		   data[1][3]=600107;
		   
		   data[2][0]="Infy";
		   data[2][1]="Sarath";
		   data[2][2]="babu";
		   data[2][3]=600106; */
		   Object[][] data = ReadExcel.readData();
		   return data;
		   
	   }
	   
	   }
