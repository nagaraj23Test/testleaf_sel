package testcases;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.TestNGTestcases;

public class MergeLeaf extends TestNGTestcases
{

@BeforeTest(groups="any")
public void setData() {
	testCaseName = "CreateLeaf";
	testDesc = "TestLeaf Webpage";
	author = "Nagarajan";
	category = "Practice";
}
@Test
public void mergeTest()
{
	WebElement createLead = locateElement("linkText","Create Lead");
	click(createLead);
	WebElement mergeTest = locateElement("linkText","Merge Leads");
    click(mergeTest);
    WebElement fromLead = locateElement("xpath","(//img[@alt='Lookup'])[1]");
    click(fromLead);
    switchToWindow(1);
    WebElement fName = locateElement("xpath","//input[@name='firstName']");
    type(fName,"Gopi");
    WebElement buttonLeads = locateElement("xpath","//button[@class='x-btn-text']");
    click(buttonLeads);
    WebElement idLink = locateElement("linkText","11043");
    click(idLink);
   // To Lead process
    WebElement toLead = locateElement("xpath","(//img[@alt='Lookup'])[2]");
   click(toLead);
   switchToWindow(1);
    WebElement lName = locateElement("xpath","//input[@name='firstName']");
    type(lName,"babu");
    WebElement buttonTo = locateElement("xpath","//button[@class='x-btn-text']");
    click(buttonTo);
    WebElement toidLink = locateElement("linkText","10192");
    click(toidLink); 
}
}
