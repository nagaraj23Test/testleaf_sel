package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.TestNGTestcases;

public class FindLeads extends TestNGTestcases{
	
	@BeforeTest(groups= {"sanity"})
	public void setData() {
		testCaseName = "FindLeads";
		testDesc = "TestLeaf Webpage";
		author = "Nagarajan";
		category = "Practice";
	}
	
	//@Test(dependsOnMethods="testcases.CreateLeaf.createLead")
	@Test(groups= {"sanity"})
	public void findLeads()
    {
		//login();
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		
    WebElement findLead = locateElement("linkText","Find Leads");
    click(findLead);
    WebElement firstNameA = locateElement("xpath","(//input[@name='firstName'])[3]");
    type(firstNameA,"Nagarajan");
    WebElement lastNameA = locateElement("xpath","(//input[@name='lastName'])[3]");
    type(lastNameA,"Ramakrishnan");
    WebElement companyNameA = locateElement("xpath","(//input[@name='companyName'])[2]");
    type(companyNameA,"Volante Technologies");
    WebElement buttonA = locateElement("xpath","//button[text()='Find Leads']");
    click(buttonA);
    }

}
