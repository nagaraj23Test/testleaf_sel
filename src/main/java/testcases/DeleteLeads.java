package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.TestNGTestcases;

public class DeleteLeads extends TestNGTestcases
{
	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName = "FindLeads";
		testDesc = "TestLeaf Webpage";
		author = "Nagarajan";
		category = "Practice";
	}
	
	//@Test(dependsOnMethods="testcases.CreateLeaf.createLead")
	@Test(groups= {"regression"})
	 public void deleteLead()
	    {
		
	    WebElement linkText = locateElement("linkText","10010");
     click(linkText);	
     WebElement DeleteLeaf = locateElement("linkText","Delete");
	    click(DeleteLeaf);
	    takeSnap();
	    }
	    //Test-Case below represents Merge Leaf.
	}


