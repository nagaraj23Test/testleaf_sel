package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsSchools {
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	String verifyText="Hello Nagarajan! How are you today?";
	driver.switchTo().frame("iframeResult");
	driver.findElementByXPath("//button[text()='Try it']").click();
	Alert alertbox = driver.switchTo().alert();
    alertbox.sendKeys("Nagarajan");
    alertbox.accept();
    String textName = driver.findElementByXPath("//p[@id='demo']").getText();
    System.out.println(textName);
    if(verifyText.equals(textName))
    {
    	System.out.println("True");
    }
    else
    {
    	System.out.println("false");
    }
    driver.switchTo().defaultContent();
}
}
