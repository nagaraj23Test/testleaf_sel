package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageScreenshot 
{
public static void main(String[] args) throws IOException {
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	driver.findElementByLinkText("Contact Us").click();
	Set<String> windowHandles = driver.getWindowHandles();
	List<String> winSize=new ArrayList<String>();
	//System.out.println("No of Windows"+winSize.size());
	winSize.addAll(windowHandles);
	driver.switchTo().window(winSize.get(1));
	System.out.println(driver.getTitle());
	String windowTitile=driver.getTitle();
	driver.switchTo().window(winSize.get(0));
	driver.close();
	driver.switchTo().window(winSize.get(1));
	System.out.println(driver.getTitle());
	
	//Taking Screenshot
	File src = driver.getScreenshotAs(OutputType.FILE);
	File obj=new File("./Snaps/img1.jpg");
	FileUtils.copyFile(src, obj);
	
}
}
