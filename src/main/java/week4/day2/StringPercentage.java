package week4.day2;

import java.util.Scanner;

public class StringPercentage {
	
public static void main(String[] args) {
	String name="Tiger Runs @ The Speed Of 100 km/hour.";
	Scanner sc= new Scanner(System.in);
	int upperCase=0;
	int lowerCase=0;
	int digits=0;
	int others=0;
	
	char[] words=name.toCharArray();
	int total=words.length;
	for(int i=0;i<words.length;i++)
	{
		if(Character.isUpperCase(words[i]))
		{
			upperCase++;
		}
		else if(Character.isLowerCase(words[i]))
		{
			lowerCase++;
		}
		else if(Character.isDigit(words[i]))
		{
			digits++;
		}
		else
		{
			others++;
		}
	}
	//System.out.println("UpperCase: "+upperCase+"total"+total);
	//double a=(upperCase/total);
	float answer= ((float)others/total)*100;
	//System.out.println(answer);
	String percentage=String.format("%.02f", answer);
	System.out.println("Percentage of the upperCase"+percentage);
			
			
}
}
