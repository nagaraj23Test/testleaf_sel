package week2.day1;

public class SmartTv extends Television
{
public void radio()
{
	System.out.println("Child radio");
}
public void pendrive()
{
	System.out.println("child pendrive");
}
public void wifi()
{
	System.out.println("child wifi");
}
@Override
public void display()
{
	//super.display();
	System.out.println("Child class display");
}
public static void main(String[] args) 
{
	Television tv=new Television();
	SmartTv s=new SmartTv();
	Entertainment e=new Television();
	Television ts=new SmartTv();
	tv.size();
	tv.frequency();
	e.frequency();
	s.radio();
	//s.display();
	ts.display();
}
}

