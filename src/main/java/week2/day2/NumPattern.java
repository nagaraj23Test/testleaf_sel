package week2.day2;

import java.io.IOException;
import java.util.Scanner;

public class NumPattern 
{
public static void main(String args[]) throws IOException
{
	int a,b,sum;
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the first number");
	a=sc.nextInt();
	System.out.println("Enter the second number:");
	b=sc.nextInt();
	
	if(a<b)
	{
		for(int i=a;i<=b;i++)
		{
			sum=i;
			if((sum%3==0) && (sum%5==0))
			{
				System.out.println("FIZZBUZZ");
			}
			else if(sum%3==0)
			{
				System.out.println("FIZZ");
			}
			else if(sum%5==0)
			{
				System.out.println("BUZZ");
			}
			
			else
			{
				System.out.println(sum);
			}
		}
}
	else
	{
		System.out.println("The first number a should be greater than b");
	}
}
}
