package week5.day1;

import java.util.Scanner;

public class DialgonalSun
{
static int rows;
static int columns;
public static void main(String[] args)
{
	Scanner sc= new Scanner(System.in);
	System.out.println("Enter the rows");
	rows=sc.nextInt();
	System.out.println("Enter the columns:");
	columns=sc.nextInt();
	int a[][]=new int[rows][columns];
	System.out.println("Enter the values:");
	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<columns;j++)
		{
			a[i][j]=sc.nextInt();
		}
	}
	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<columns;j++)
		{
			System.out.print(a[i][j]);
			System.out.print("\t");
		}
		System.out.println("");
	}
	
	beforePivot(a);
	afterPivot(a);
}
public static void beforePivot(int[][] a) {
	int temp[][]=a;
	int sum=0;
	int columnValue=temp[1].length;
	for(int i=0;i<a.length;i++)
	{
		for(int j=0;j<columnValue;j++)
		{
			sum=sum+temp[i][j];
			columnValue--;
		}
	}
	System.out.println("Before sum: "+sum);
	//System.out.print(sum);
}

public static void afterPivot(int[][] a) 
{
	int temp[][]=a;
	int sum=0;
	int columnValue=temp[1].length-1;
	//System.out.println("Column value "+columnValue);
	//System.out.println("J array length"+temp[1].length);
	for(int i=1;i<a.length;i++)
	{
		
		for(int j=columnValue+1;j<=temp[1].length;j++)
		{
			
			
			sum=sum+temp[i][j-1];
			columnValue--;
		}
	}
	System.out.println("After sum: "+sum);
	//System.out.print(sum);
}

}
