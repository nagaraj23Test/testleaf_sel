package week5.day1;

public class SpiralMatrix 
{
public static void main(String[] args)
{
	int temp=1;
	int [][] a=new int [4][4];
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++,temp++)
		{
			a[i][j]=temp;
			System.out.print(temp);
			System.out.print("\t");
		}
		System.out.println("");
	}
	int arrSize=a.length;
	leftToRight(a);
	topDown(a);
	rightLeft(a);
	bottomUp(a);
	secondLeftRight(a);
	secondRightLeft(a);
	//System.out.println(a[1][1]);
	
}

public static void leftToRight (int a[][])
{
	
	int[][] temp = a;
	int d[][]=new int[1][3];
	for(int i=0;i<1;i++)
	{
		for(int j=0;j<3;j++)
		{
			d[i][j]=temp[i][j];
			
		}
	}
	//To print the array
	for(int i=0;i<1;i++)
	{
		for(int j=0;j<3;j++)
		{
	System.out.print(d[i][j]);
			System.out.print("\t");
		}
	}
}

public static void topDown (int a[][])
{
	int[][] temp = a;
	int d[][]=new int[3][3];
	//System.out.println("");
	for(int i=0;i<3;i++)
	{
		for(int j=3;;)
		{
			//d[i][j]=temp[i][j];
			System.out.print(temp[i][j]);
			System.out.print("\t");
			break;
		}
	}
	
}

public static void rightLeft (int a[][])
{
	int[][] temp = a;
	int d[][]=new int[3][3];
	//System.out.println("");
	for(int i=3;;)
	{
		for(int j=3;j>0;j--)
		{
			//d[i][j]=temp[i][j];
			System.out.print(temp[i][j]);
			System.out.print("\t");
			
		}
		break;
	}
	
}

public static void bottomUp (int a[][])
{
	int[][] temp = a;
	int d[][]=new int[3][3];
	//System.out.println("");
	for(int i=3;i>0;i--)
	{
		for(int j=0;;)
		{
			//d[i][j]=temp[i][j];
			System.out.print(temp[i][j]);
			System.out.print("\t");
			break;
		}
		
	}
	
}

public static void secondLeftRight (int a[][])
{
	int[][] temp = a;
	int d[][]=new int[3][3];
	//System.out.println("");
	for(int i=1;i<2;i++)
	{
		for(int j=1;j<3;j++)
		{
			//d[i][j]=temp[i][j];
			System.out.print(temp[i][j]);
			System.out.print("\t");
			//break;
		}
		
	}
	
}

public static void secondRightLeft (int a[][])
{
	int[][] temp = a;
	int d[][]=new int[3][3];
	//System.out.println("");
	for(int i=2;;)
	{
		for(int j=2;j>0;j--)
		{
			//d[i][j]=temp[i][j];
			System.out.print(temp[i][j]);
			System.out.print("\t");
			
		}
		break;
	}
	
}


}
