package projectDay;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethods.TestNGTestcases;

public class ZoomCarLoad extends SeMethods
{
	@BeforeTest()
	public void setData() {
		testCaseName = "TC001_ZoomCarLoading";
		testDesc = "Zoomcar webpage";
		author = "Nagarajan";
		category = "Project Day";
	}
		
	
	@Test
	public void loadPage()
	{
		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement journeyLink = locateElement("xpath","//a[@class='search']");
		click(journeyLink);
		WebElement locateLocation = locateElement("xpath","//div[@class='component-popular-locations']//div[2]");
        String place=locateLocation.getText();
		click(locateLocation);	
		WebElement searchLink = locateElement("xpath","//input[@class='search']");
	    String searchText=searchLink.getText();
	  /*  if(place.equals(searchText))
	    {
	    	
            click(nextButton);	    
	    }*/
	    
	    
	    verifyPartialText(locateLocation, searchText );
	    WebElement nextButton = locateElement("xpath","//button[@class='proceed']");
        click(nextButton);
	WebElement dayPicked = locateElement("xpath","//div[@class='days']//div");
	//  below code is Date conversion
	String day=dayPicked.getText();
	String replaceDay=day.replaceAll("\\D", "");
	//System.out.println(replaceDay);
	int tday=Integer.parseInt(replaceDay);
	int j = ++tday;
	// code ended
	// passing the integer value to XPath
	//System.out.println(j);
	String nxt=Integer.toString(j);
	//System.out.println(nxt);
	WebElement nextDay = locateElement("xpath","//div[contains(text(),'"+nxt+"')]");
	
	click(nextDay);
	//Date coded ended
	WebElement clickNxt = locateElement("xpath","//button[@class='proceed']");
	click(clickNxt);
	//Verify the date
	WebElement confirmDate = locateElement("xpath","//div[@class='days']//div");
	String confirmDay=confirmDate.getText();
	//Confirm date
	/*if(nxt.contains(confirmDay)) {
		System.out.println("Date Confirmed");
	}*/
	WebElement doneClick = locateElement("xpath","//button[@class='proceed']");
	click(doneClick);
	
	// Select the car with maximum price
	String rupee;
	List<WebElement> carComponents = locateElements("xpath", "//div[@class='price']");
	List<Integer> priceList=new ArrayList<Integer>();
	//System.out.println(carComponents.size());
		for(WebElement price:carComponents)
	{
		String priceText=price.getText();
		rupee=priceText.replaceAll("\\D", "");
		//System.out.println(rupee);
		int a=Integer.parseInt(rupee);
		priceList.add(a);
	}
		Collections.sort(priceList);
		//System.out.println(priceList.get(priceList.size()-1));
	//	System.out.println(priceList);
	int priceMax=priceList.get(priceList.size()-1);
	System.out.println(priceMax);
	WebElement carDetails = locateElement("xpath","//div[contains(text(),'"+priceMax+"')]/../../..//following::div/h3");
	String carInfo=carDetails.getText();
	System.out.println(carInfo);
	
	//Book car
	WebElement clickBook = locateElement("xpath","//div[contains(text(),'552')]/following-sibling::button[@class='book-car']");
	click(clickBook);
	}

}
