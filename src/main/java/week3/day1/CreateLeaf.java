package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLeaf 
{
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://leaftaps.com/opentaps");
	//Username
	driver.findElementById("username").sendKeys("DemoSalesManager");
	//password
	driver.findElementById("password").sendKeys("crmsfa");
	//click button
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Create Lead").click();
	driver.findElementById("createLeadForm_companyName").sendKeys("Volante");
	driver.findElementById("createLeadForm_firstName").sendKeys("Nagarajan");
	driver.findElementById("createLeadForm_lastName").sendKeys("R");
	WebElement ele=driver.findElementById("createLeadForm_dataSourceId");
	Select eachSelect=new Select(ele);
	List<WebElement> options = eachSelect.getOptions();
	eachSelect.selectByIndex(2);
	eachSelect.selectByVisibleText("Cold Call");
	Select Industry=new Select(driver.findElementById("createLeadForm_industryEnumId"));
	Industry.selectByVisibleText("Finance");
	// To display all the text in the drop-down
	Select owner=new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
	List<WebElement> own = owner.getOptions();
	System.out.println("All the owner names are below:");
	for(WebElement elt:own)
	{
		System.out.println(elt.getText());
	}
	System.out.println("The name start with P:");
	//for(WebElement elt:)
}
}
