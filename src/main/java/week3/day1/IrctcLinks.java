package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcLinks {
public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	// case 1: Test the Check availability link, get the different error message.
	//Case 2: Provide non-allowed characters, and check boundary value analysis.
	//Case 3: Select the value in the drop-down list.
	//Casae 4: Check the radio button
	//Case 5: Check country name and ISD Mobile range.
	//Case 5: Select the Nationality against the country.
	
	//To check the number of links and the click on the corresponding link.
	/*List<WebElement> tagsName = driver.findElementsByTagName("a");
	int a=tagsName.size();
	System.out.println(a);
	String Link="Book Meal";
	for(WebElement nameLink:tagsName)
		
	{
		//System.out.println(nameLink.getText());
		if(nameLink.getText().equals(Link))
		{
			driver.findElementByLinkText(Link).click();
		}
	}*/
	//Case 1.a //Existing User
	driver.findElementById("userRegistrationForm:userName").sendKeys("Naaga");
	driver.findElementById("userRegistrationForm:checkavail").click();
	Thread.sleep(3000);
	String text2 = driver.findElementById("userRegistrationForm:useravail").getText();
	System.out.println("The error message: "+text2);
	
	//Case1.b - Invalid User
	driver.findElementById("userRegistrationForm:userName").clear();
	driver.findElementById("userRegistrationForm:userName").sendKeys("12365a");
	driver.findElementById("userRegistrationForm:checkavail").click();
	Thread.sleep(3000);
	String text = driver.findElementById("userRegistrationForm:useravail").getText();
	System.out.println("The invalid Text"+text);
	
	//Case 3:
   //Drop-down List using Select Statement
	WebElement ele = driver.findElementById("userRegistrationForm:prelan");
    Select st= new Select(ele);
    List<WebElement> drpdwn = st.getOptions();
for(WebElement drp:drpdwn)
{
	System.out.println(drp.getText());
}
//Case 4:
//To get country pincode number
WebElement country = driver.findElementById("userRegistrationForm:countries");
Select ct=new Select(country);
List<WebElement> ctOptions = ct.getOptions();
ct.selectByVisibleText("Australia");
		 String code=driver.findElementById("userRegistrationForm:isdCode").getAttribute("value");
		System.out.println(code);

}
}
