package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class eRail 
{
public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://erail.in/");
	driver.findElementById("txtStationFrom").clear();
	driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
	driver.findElementById("txtStationTo").clear();
	driver.findElementById("txtStationTo").sendKeys("MALM",Keys.TAB);
	WebElement ele = driver.findElementById("chkSelectDateOnly");
	if(ele.isSelected())
	{
		ele.click();
	}
	Thread.sleep(3000);
	WebElement schdle = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	List<WebElement> rows = schdle.findElements(By.tagName("tr"));
for(WebElement row:rows)
{
	System.out.println(row.getText());
}
for(int i=0;i<rows.size();i++)
{
	List<WebElement> col = rows.get(i).findElements(By.tagName("td"));
	String text=col.get(i).getText();
	System.out.println(text);
}
}
}
