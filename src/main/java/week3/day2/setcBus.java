package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class setcBus 
{
	public static void main(String[] args) throws InterruptedException {
		
	
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://www.tnstc.in/TNSTCOnline/");
	driver.findElementByXPath("//input[@name='txtUserLoginID']").sendKeys("siva.mrsiva");
	driver.findElementByXPath("//input[@name='txtPassword']").sendKeys("Siva1siva");
	List<WebElement> tag = driver.findElementsByTagName("a");
	String Link="Sign In";
	//Thread.sleep(5000);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	for(WebElement tagName:tag)
	{
		//System.out.println(tagName.getText());
		if(tagName.getText().equals(Link))
{
			//Thread.sleep(5000);
	driver.findElementByLinkText(Link).click();
	break;
}
	}
	//Thread.sleep(5000);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	//Handling AUTO Suggestion From Place
	
	driver.findElementByXPath("//input[@id='matchStartPlace']").sendKeys("chenn");
	List<WebElement> autoSuggest = driver.findElementsByXPath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all']//li/a");
	for(WebElement auto:autoSuggest)
	{
		//System.out.println(auto.getText());
	if(auto.getText().equals("CHENNAI CMBT"))
	{
		driver.findElementByLinkText("CHENNAI CMBT").click();
	}
	}
	
	//Auto Suggestion handling to Place
	driver.findElementByXPath("//input[@id='matchEndPlace']").sendKeys("Mad");
	List<WebElement> destination = driver.findElementsByXPath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all'][2]//li/a");
	for(WebElement dest:destination)
	{
		if(dest.getText().equals("MADURAI"))
		{
			driver.findElementByLinkText("MADURAI").click();
		}
	}
	//Thread.sleep(4000);
	List<WebElement> srch = driver.findElementsByTagName("a");
    String clicks="SEARCH AVAILABLE SERVICES";
    for(WebElement sr:srch)
    {
    	if(sr.getText().equals(clicks))
    	{
    driver.findElementByLinkText(clicks).click();
    		break;
    	}
    	//System.out.println(sr.getText());
    }
	
	/*WebDriverWait wait=new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@id='ui-id-1']")));
	driver.findElementByXPath("//ul[@id='ui-id-1']").click();*/
//Table Data
    WebElement tableTime = driver.findElementByXPath("//table[@class='bgColor']");
	List<WebElement> tableRows = tableTime.findElements(By.tagName("tr"));
	for(WebElement row:tableRows)
	{
		System.out.println(row.getText());
	}
	}
}