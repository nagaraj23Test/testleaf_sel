package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		WebElement drp = driver.findElementById("userRegistrationForm:countries");
	    Select st = new Select(drp);
	    List<WebElement> countries = st.getOptions();
	    System.out.println("The countries in the dropdown are follows");
	    for(WebElement cnt:countries)
	    {
	    	System.out.println(cnt.getText());
	    }
	}

}
