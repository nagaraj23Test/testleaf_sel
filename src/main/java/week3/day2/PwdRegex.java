package week3.day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PwdRegex 
{
public static void main(String[] args) {
	String pwd;
	Scanner sc=new Scanner(System.in);
	String txt = "abcno";
	//String pattern = "[A-Z]{5}[0-9]{4}[A-Z]";
	//String pattern = "[^abc]{5}";
	String pattern = "[a-d[m-p]]{}";
	
	Pattern compile = Pattern.compile(pattern);
	Matcher matcher = compile.matcher(txt);
	System.out.println(matcher.matches());

}
}
