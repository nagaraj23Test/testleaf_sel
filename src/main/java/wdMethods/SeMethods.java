package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./Driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//Edited with report code
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("Pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			//edited with report step
			reportStep("fail", "The Browser "+browser+" not Launched ");
			//System.out.println("The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			
			switch(locator) {
			case "id": 	return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "name": return driver.findElementByName(locValue);
			case "partialLinkText": return driver.findElementByPartialLinkText(locValue);
			case "linkText": return driver.findElementByLinkText(locValue);
			case "tag": return driver.findElementByTagName(locValue);
			case "cssSelector": return driver.findElementByCssSelector(locValue);
			}
			
		}catch (NullPointerException e) {
			// TODO: handle exception
		} 
		
		catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
			throw new NoSuchElementException("The Element Is Not Located ");
		}catch (WebDriverException e) {
			System.out.println("The Element Is Not Located ");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.out.println("The Element Is Not Located ");
			throw new RuntimeException();
		}
		return null;
	}
	
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			
			switch(locator) {
			case "id": 	return driver.findElementsById(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "xpath": return driver.findElementsByXPath(locValue);
			case "name": return driver.findElementsByName(locValue);
			case "partialLinkText": return driver.findElementsByPartialLinkText(locValue);
			case "linkText": return driver.findElementsByLinkText(locValue);
			case "tag": return driver.findElementsByTagName(locValue);
			case "cssSelector": return driver.findElementsByCssSelector(locValue);
			}
			
		}catch (NullPointerException e) {
			// TODO: handle exception
		} 
		
		catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
			throw new NoSuchElementException("The Element Is Not Located ");
		}catch (WebDriverException e) {
			System.out.println("The Element Is Not Located ");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.out.println("The Element Is Not Located ");
			throw new RuntimeException();
		}
		return null;
	}

	
	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}
	
	@Override
	public void type(WebElement ele, String data) {
	try
	{
		ele.sendKeys(data);
		reportStep("pass", "The Data "+data+" is Entered Successfully");
	}
		catch(Exception e)
	{
			reportStep("fail", "The Data "+data+" Not Entered");
	}
	takeSnap();
	}
	
	public void typeEnter(WebElement ele, String data) {
		try
		{
			ele.sendKeys(data,Keys.ENTER);
			reportStep("pass", "The Data "+data+" is Entered Successfully");
		}
			catch(Exception e)
		{
				reportStep("fail", "The Data "+data+" Not Entered");
		}
		takeSnap();
		}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("pass", "The Data "+ele+" is Entered Successfully");
			//System.out.println("The Element "+ele+" Clicked Successfully");
			
		} catch (ElementNotVisibleException e) {
			reportStep("fail", "The Data "+ele+" Not Entered");
		}
		takeSnap();
	}
	public void clickWithOutSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("pass", "The Data "+ele+" is Entered Successfully");
			//System.out.println("The Element "+ele+" Clicked Successfully");
			
		} catch (ElementNotVisibleException e) {
			reportStep("fail", "The Data "+ele+" Not Entered");
		}
		takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		
		try{
			Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		//System.out.println("The DropDown Is Selected with "+value);
		reportStep("pass", "The Data "+ele+" and "+value+"is Entered Successfully");
	}
		catch (ElementNotVisibleException e) {
			reportStep("fail", "The Data "+ele+" Not Entered");
		}
	}
	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		try {		
		reportStep("pass", "The Title verified Successfully");
		return true;
		}
		catch (NoSuchElementException e) {
			reportStep("fail", "The title is in correct");
			return false;
		}
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

		String value=ele.getText();
		if(value.contains(expectedText))
		{
			System.out.println("Text verified");
		}
		else
		{
			System.out.println("Not Verified");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./Snap/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
      driver.quit();
	}


	@Override
	public WebElement tableSelection(String ele, String value) {
		try
		{
		WebElement tableTime = driver.findElementByXPath("//div[@class='"+ele+"']");
		System.out.println("class found");
		List<WebElement> tableRows = tableTime.findElements(By.tagName("td"));
		for(WebElement row:tableRows)
		{
			String text=row.getText();
			//System.out.println(text);
			
			if(text.equals(value))
			{
				return row;
			}
		}
		}
		catch(Exception e)
		{
			System.out.println("Element not found");
		}
		return null;
	}


	
}
