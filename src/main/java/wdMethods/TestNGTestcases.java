package wdMethods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class TestNGTestcases extends SeMethods
{
	@Parameters({"url","username","password"})
	//@BeforeMethod(groups="any")
	@BeforeMethod
	public void login(String url, String username, String password)
	{
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleLink = locateElement("linkText","CRM/SFA" );
		click(eleLink);		
	}
	//@AfterMethod(groups="any")
	@AfterMethod
	public void closeBrowsers()
	{
		closeBrowser();
	}

}
