package test.week4;

import java.util.Arrays;

public class MaxMin
{
public static void main(String[] args)
{
	int arr[]=new int[] {11,13,3,7,18,8,9};
	Arrays.sort(arr);
	System.out.println("Minimum Value: "+arr[0]);
	System.out.println("Maximum value: "+arr[arr.length-1]);
}
}
