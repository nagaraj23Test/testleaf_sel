package test.week4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IphoneX
{
public static void main(String[] args) {
	try
	{
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.flipkart.com/");
	driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElementByXPath("//input[@name='q']").sendKeys("IphoneX");
	driver.findElementByXPath("//button[@type='submit']").click();
	WebElement text=driver.findElementByXPath("(//div[@class='_3wU53n'])[2]");
	System.out.println(text.getText());
	driver.findElementByXPath("(//div[@class='_3wU53n'])[2]").click();

	WebElement price = driver.findElementByXPath("//div[@class='_1uv9Cb']");
	String priceText=price.getText();
	String finalPrice = priceText.replaceAll("\\D", "");
	System.out.println("The price of the phone: "+finalPrice.substring(0, 6));
	}
catch(NoSuchElementException e)
	{
	System.out.println("No such element found");
	
	}
}
}
